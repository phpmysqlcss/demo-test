<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'database.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// GET DATA FORM REQUEST

//CREATE MESSAGE ARRAY AND SET EMPTY

	if(isset($_FILES['file'])){ 	 
		 //echo json_encode($_FILES);	
			$uploaddir = 'upload/';
			$uploadfile = $uploaddir . time().'_'.basename($_FILES['file']['name']); 

			if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
				$data = file_get_contents($uploadfile, true);
				/////////
				$alldata = json_decode($data);
				foreach ($alldata as $allrow) {
					foreach ($allrow as  $value) { 
						$mid = $value->manufacturer->id; $mname = $value->manufacturer->name;
						/////////////////////////////////////	insert manufacturer					
						  $msql = is_numeric($mid) ? "SELECT * FROM `manufacturer` WHERE id='$mid'" : "SELECT * FROM `manufacturer`"; 
									$mstmt = $conn->prepare($msql);
									$mstmt->execute();							
							if($mstmt->rowCount() > 0){
								//$msg['message'] = '';//Already Exists
							}else{							
								$insert_query = "INSERT INTO `manufacturer`(`id`, `name`) VALUES(:id,:name)";        
								$insert_stmt = $conn->prepare($insert_query);
								// DATA BINDING
								$insert_stmt->bindValue(':id', htmlspecialchars(strip_tags($mid)),PDO::PARAM_STR); 
								$insert_stmt->bindValue(':name', htmlspecialchars(strip_tags($mname)),PDO::PARAM_STR);
								$insert_stmt->execute();
							}
						/////////////////////////////////////
						$article_number = $value->article_number;
							$sql = is_numeric($article_number) ? "SELECT * FROM `articles` WHERE article_number='$article_number'" : "SELECT * FROM `articles`"; 
							$stmt = $conn->prepare($sql);
							$stmt->execute();							
						if($stmt->rowCount() > 0){
							$msg['message'] = 'Already Exists';
						}else{							
							$insert_query = "INSERT INTO `articles`(`article_number`, `manufacturer_id`, `text1_de`, `text1_en`, `sku`, `created`, `shop_status`, `hidden`, `deleted`, `weight`, `text2_de`, `text2_en`, `ean`, `ek`, `vk1`, `vk2`, `vk3`, `vk4`, `vk5`, `vk6`, `stock`, `replenishment_time`, `tdp`, `available_from`, `active_from`, `active_until`, `puid`, `pdf_link`, `length`, `width`, `depth`, `deeplink`, `long_description_de`, `long_description_en`) VALUES(:article_number,:manufacturer_id,:text1_de,:text1_en,:sku,:created,:shop_status,:hidden,:deleted,:weight,:text2_de,:text2_en,:ean,:ek,:vk1,:vk2,:vk3,:vk4,:vk5,:vk6,:stock,:replenishment_time,:tdp,:available_from,:active_from,:active_until, :puid,:pdf_link,:length,:width,:depth,:deeplink,:long_description_de,:long_description_en)";        
							$insert_stmt = $conn->prepare($insert_query);
							// DATA BINDING
							$insert_stmt->bindValue(':article_number', htmlspecialchars(strip_tags($value->article_number)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':manufacturer_id', htmlspecialchars(strip_tags($value->manufacturer->id)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':text1_en', htmlspecialchars(strip_tags($value->text1_en)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':text1_de', htmlspecialchars(strip_tags($value->text1_de)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':sku', htmlspecialchars(strip_tags($value->sku)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':created', htmlspecialchars(strip_tags($value->created)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':shop_status', htmlspecialchars(strip_tags($value->shop_status)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':hidden', htmlspecialchars(strip_tags($value->hidden)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':deleted', htmlspecialchars(strip_tags($value->deleted)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':weight', htmlspecialchars(strip_tags($value->weight)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':text2_de', htmlspecialchars(strip_tags($value->text2_de)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':text2_en', htmlspecialchars(strip_tags($value->text2_en)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':ean', htmlspecialchars(strip_tags($value->ean)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':ek', htmlspecialchars(strip_tags($value->ek)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':vk1', htmlspecialchars(strip_tags($value->vk1)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':vk2', htmlspecialchars(strip_tags($value->vk2)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':vk3', htmlspecialchars(strip_tags($value->vk3)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':vk4', htmlspecialchars(strip_tags($value->vk4)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':vk5', htmlspecialchars(strip_tags($value->vk5)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':vk6', htmlspecialchars(strip_tags($value->vk6)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':stock', htmlspecialchars(strip_tags($value->stock)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':replenishment_time', htmlspecialchars(strip_tags($value->replenishment_time)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':tdp', htmlspecialchars(strip_tags($value->tdp)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':available_from', htmlspecialchars(strip_tags($value->available_from)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':active_from', htmlspecialchars(strip_tags($value->active_from)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':active_until', htmlspecialchars(strip_tags($value->active_until)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':puid', htmlspecialchars(strip_tags($value->puid)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':pdf_link', htmlspecialchars(strip_tags($value->pdf_link)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':length', htmlspecialchars(strip_tags($value->length)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':width', htmlspecialchars(strip_tags($value->width)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':depth', htmlspecialchars(strip_tags($value->depth)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':deeplink', htmlspecialchars(strip_tags($value->deeplink)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':long_description_de', htmlspecialchars(strip_tags($value->long_description_de)),PDO::PARAM_STR);
							$insert_stmt->bindValue(':long_description_en', htmlspecialchars(strip_tags($value->long_description_en)),PDO::PARAM_STR);
							
							if($insert_stmt->execute()){
								$msg['message'] = 'Data Inserted Successfully';
							}else{
								$msg['message'] = 'Data not Inserted';
							} 
						}
					}
					 
				}
			  //////////// end foreach
			} else {
				
			    $msg['message'] = "Upload failed";
			   
			}
 
			 echo json_encode($msg); 
	}
	else{
		echo json_encode(array("msg"=>"404 Page Not Found!"));
	}
 
	 
?>