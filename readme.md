 
## Developement Tools

<ul>
<li>Raw PHP</li> 
<li>PHP API</li> 
</ul> 

## Requirements

<ul>
<li>PHP > 7</li> 
<li>Change  baseurl in htaccess</li>
<li>connect Database properly</li>  
</ul> 

## API List
<ul>
<li>Insert  : http://localhost/allwork/demo-ecommerce/insert.php 
	<img src="doc-img/insert_api_demo.png">
</li> 
<li>View All List :  http://localhost/allwork/demo-ecommerce/read.php 
	<img src="doc-img/all_data_view_api.png"></li> 

<li>View Sigle Data :  http://localhost/allwork/demo-ecommerce/read/1216129 
	<img src="doc-img/single_data_view_api.png"></li> 

<li>Delete :  http://localhost/allwork/demo-ecommerce/delete.php 
	<img src="doc-img/deleted_single_data_api.png"></li> 

</ul> 


## Developer
This project is developed By Md Wasim
 
