-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 23, 2020 at 10:46 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo_ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `article_number` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `text1_de` varchar(255) DEFAULT 'NULL',
  `text1_en` varchar(255) DEFAULT 'NULL',
  `sku` varchar(255) DEFAULT NULL,
  `created` date NOT NULL,
  `shop_status` int(11) NOT NULL DEFAULT 1,
  `hidden` varchar(255) NOT NULL DEFAULT 'false',
  `deleted` varchar(255) NOT NULL DEFAULT 'false',
  `weight` decimal(10,2) NOT NULL DEFAULT 0.00,
  `text2_de` varchar(355) DEFAULT NULL,
  `text2_en` varchar(355) DEFAULT NULL,
  `ean` varchar(255) DEFAULT NULL,
  `ek` decimal(10,2) NOT NULL DEFAULT 0.00,
  `vk1` decimal(10,2) NOT NULL DEFAULT 0.00,
  `vk2` decimal(10,2) NOT NULL DEFAULT 0.00,
  `vk3` decimal(10,2) NOT NULL DEFAULT 0.00,
  `vk4` decimal(10,2) NOT NULL DEFAULT 0.00,
  `vk5` decimal(10,2) NOT NULL DEFAULT 0.00,
  `vk6` decimal(10,2) NOT NULL DEFAULT 0.00,
  `stock` int(11) NOT NULL,
  `replenishment_time` int(11) NOT NULL,
  `tdp` varchar(255) DEFAULT NULL,
  `available_from` varchar(255) DEFAULT NULL,
  `active_from` varchar(255) DEFAULT NULL,
  `active_until` varchar(255) DEFAULT NULL,
  `puid` varchar(255) NOT NULL,
  `pdf_link` varchar(255) DEFAULT NULL,
  `length` varchar(255) DEFAULT NULL,
  `width` varchar(255) DEFAULT NULL,
  `depth` varchar(255) DEFAULT NULL,
  `deeplink` varchar(255) NOT NULL,
  `long_description_de` varchar(355) DEFAULT NULL,
  `long_description_en` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`article_number`, `manufacturer_id`, `text1_de`, `text1_en`, `sku`, `created`, `shop_status`, `hidden`, `deleted`, `weight`, `text2_de`, `text2_en`, `ean`, `ek`, `vk1`, `vk2`, `vk3`, `vk4`, `vk5`, `vk6`, `stock`, `replenishment_time`, `tdp`, `available_from`, `active_from`, `active_until`, `puid`, `pdf_link`, `length`, `width`, `depth`, `deeplink`, `long_description_de`, `long_description_en`) VALUES
(1216129, 1120, 'Dell Server R620', 'DELL SERVER R620', 'SYS-4029GP-TRT', '2018-03-21', 1, '', '', '46.00', '', '', '', '3603.94', '4510.00', '4342.10', '4095.39', '4030.00', '3965.00', '3896.00', 5, 3, '', '', '', '', '5950841000', '', '', '', '', 'https://www.dell.com/red/app#products/page/5950841000/-', '', ''),
(1216130, 1121, 'Supermicro Barebone 4029GP-TRT2', 'Supermicro barebone 4029GP-TRT2', 'SYS-4029GP-TRT2', '2018-03-21', 1, '', '', '37.00', '', '', '', '3947.50', '4940.00', '4756.03', '4485.80', '4415.00', '4340.00', '4268.00', 1, 3, '', '', '', '', '5950842000', '', '', '', '', 'https://www.smicro.com/red/app#search/portal/SYS-4029GP-TR', '', ''),
(1216131, 1121, 'Supermicro Barebone 2029P-C1R', 'Supermicro barebone 2029P-C1R', 'SYS-2029P-C1R', '2018-03-21', 1, '', '', '18.00', '', '', '', '1515.04', '1900.00', '1825.35', '1721.64', '1695.00', '1665.00', '1638.00', 1, 3, '', '', '', '', '6004369000', '', '', '', '', 'https://www.smicro.com/red/app#search/portal/SYS-2029P-C1R/', '', ''),
(1216132, 1121, 'Supermicro Barebone 2029P-C1R', 'Supermicro barebone 2029P-C1R', 'SYS-2029P-C1R', '2018-03-21', 1, '', '', '18.00', '', '', '', '1515.04', '1900.00', '1825.35', '1721.64', '1695.00', '1665.00', '1638.00', 1, 3, '', '', '', '', '6004369000', '', '', '', '', 'https://www.smicro.com/red/app#search/portal/SYS-2029P-C1R/', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE `manufacturer` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manufacturer`
--

INSERT INTO `manufacturer` (`id`, `name`) VALUES
(1120, 'Dell'),
(1121, 'SUPER MICRO Computer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`article_number`);

--
-- Indexes for table `manufacturer`
--
ALTER TABLE `manufacturer`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `article_number` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1216133;

--
-- AUTO_INCREMENT for table `manufacturer`
--
ALTER TABLE `manufacturer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1122;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
