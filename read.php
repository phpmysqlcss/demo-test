<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=UTF-8");

// INCLUDING DATABASE AND MAKING OBJECT
require 'database.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// CHECK GET ID PARAMETER OR NOT
if(isset($_GET['article_number']))
{
    //IF HAS ID PARAMETER
    $article_number = filter_var($_GET['article_number'], FILTER_VALIDATE_INT,[
        'options' => [
            'default' => 'all_posts',
            'min_range' => 1
        ]
    ]);
}
else{
    $article_number = 'all_posts';
}

// MAKE SQL QUERY
// IF GET POSTS ID, THEN SHOW POSTS BY ID OTHERWISE SHOW ALL POSTS
$sql = is_numeric($article_number) ? "SELECT * FROM `articles` WHERE article_number='$article_number'" : "SELECT * FROM `articles`"; 
$stmt = $conn->prepare($sql);
$stmt->execute();

//CHECK WHETHER THERE IS ANY POST IN OUR DATABASE
if($stmt->rowCount() > 0){
    // CREATE POSTS ARRAY
    $articles_array = [];
    
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){ 
		$msql = "SELECT * FROM `manufacturer` WHERE id='$row[manufacturer_id]'"; 
		$mstmt = $conn->prepare($msql);
		$mstmt->execute();
		$mrow = $mstmt->fetch(PDO::FETCH_ASSOC);
        $mdata = [
            'id' => $mrow['id'],
            'name' => $mrow['name']
		];
		
        $articles_data = [
            'article_number' => $row['article_number'],
            'manufacturer' => $mdata,
            'text1_de' => $row['text1_de'],
            'text1_en' => $row['text1_en'],
            'sku' => $row['sku'],
            'created' => $row['created'],
            'shop_status' => $row['shop_status'],
            'hidden' => $row['hidden'],
            'deleted' => $row['deleted'],
            'weight' => $row['weight'],
            'text2_de' => $row['text2_de'],
            'text2_en' => $row['text2_en'],
            'text1_de' => $row['ean'],
            'ean' => $row['text1_en'],
            'ek' => $row['ek'],
            'vk1' => $row['vk1'],
            'vk2' => $row['vk2'],
            'vk3' => $row['vk3'],
            'vk4' => $row['vk4'],
            'vk5' => $row['vk5'],
            'vk6' => $row['vk6'],
            'stock' => $row['stock'],
            'replenishment_time' => $row['replenishment_time'],
            'replenishment_time' => $row['replenishment_time'],
            'tdp' => $row['tdp'],
            'available_from' => $row['available_from'],
            'active_from' => $row['active_from'],
            'active_until' => $row['active_until'],
            'puid' => $row['puid'],
            'pdf_link' => $row['pdf_link'],
            'length' => $row['length'],
            'width' => $row['width'],
            'depth' => $row['depth'],
            'deeplink' => $row['deeplink'],
            'long_description_de' => $row['long_description_de'],
            'long_description_en' => $row['long_description_en']
        ];
        // PUSH POST DATA IN OUR $posts_array ARRAY
        array_push($articles_array, $articles_data);
    }
    //SHOW POST/POSTS IN JSON FORMAT
    echo json_encode($articles_array);
 

}
else{
    //IF THER IS NO POST IN OUR DATABASE
    echo json_encode(['message'=>'No post found']);
}
?>